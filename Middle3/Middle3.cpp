﻿// Middle3.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <vector>
using namespace std;

class Vehicle
{
public:
    virtual void print(ostream& os) const = 0;
    
    friend ostream& operator<<(ostream& os, const Vehicle& v)
    {
        v.print(os);
        return os;
    }
};

class WaterVehicle : public Vehicle
{
public:
    float draft;

    WaterVehicle() :draft(0.f) {}

    WaterVehicle(float d)
    {
        draft = d;
    }

    void print(ostream& os) const override
    {
        os << "WaterVehicle draft: " << draft ;
    }
};


class RoadVehicle : public Vehicle
{
public:
    float clearance;
};

class Wheel
{
public:
    float diameter;

    Wheel() :diameter(0.f) {}

    Wheel(float d)
    {
        diameter = d;
    }

};

class Engine
{
public:
    float power;
    Engine() :power(0.f) {}
   Engine(float p)
    {
        power = p;
    }

   float getPower()
   {
       return power;
   };
};

class Bicycle : public RoadVehicle
{
public:
    Wheel frontWheel;
    Wheel backWheel;

    Bicycle(const Wheel& fw, const Wheel& bw, float ch)
    {
        frontWheel = fw;
        backWheel = bw;
        clearance = ch;
    }

    void print(ostream& os) const override
    {
        os << "Bicycle clearance: " << clearance << ", front wheel diameter: " << frontWheel.diameter << ", rear wheel diameter: " << backWheel.diameter;
    }
};

class Car:public RoadVehicle
{
public:
    Wheel frontWheelL;
    Wheel frontWheelR;
    Wheel backWheelL;
    Wheel backWheelR;
    Engine engine;

    Car(const Engine& e, const Wheel& flw, const Wheel& frw, const Wheel& blw, const Wheel& brw, float ch) 
    {
        engine = e;
        frontWheelL = flw; 
        frontWheelR= frw;
        backWheelL= blw;
        backWheelR=brw;
        clearance = ch;
    }

    

    void print(ostream& os) const override
    {
        os << "Car Engine: " << engine.power << " Wheels:" << frontWheelL.diameter << " " << frontWheelR.diameter << " " << backWheelL.diameter << " " << backWheelR.diameter << " Ride height: " << clearance;
    }

    

};


class Point 
{

private:
    double x, y, z;
public:
    
    Point() :x(0), y(0), z(0) {}
    Point(double x, double y, double z) : x(x), y(y), z(z) {}
    double getX() const { return x; }
    double getY() const { return y; }
    double getZ() const { return z; }
    
};

class Circle :public Vehicle
{
private:
    Point center;
    double radius;
public:
    Circle():center(), radius(0){}
    Circle(const Point& center, double radius) : center(center), radius(radius) {}
    
    void print(ostream& os) const override
    {
        os << "Circle coord: " << center.getX()<<" "<< center.getY() <<" "<< center.getZ() <<" Radius: "<< radius;
    }
};



float getHighestPower(const vector<Vehicle*>& v) 
{
    float maxPower = 0.f;

    for (const auto& vehicle : v)
    {
        Car* car = dynamic_cast<Car *>(vehicle);
        if (car && car->engine.getPower()> maxPower)
        {
            maxPower = car->engine.getPower();
        }
    };
    
    return maxPower;
}






int main()
{
    Car c(Engine(150), Wheel(17), Wheel(17), Wheel(18), Wheel(18), 150.f);
    std::cout << c << '\n';
    Bicycle t(Wheel(20), Wheel(20), 300);
    std::cout << t << '\n';


    vector<Vehicle*> v;

    v.push_back(new Car(Engine(150), Wheel(17), Wheel(17), Wheel(18), Wheel(18), 250));

    v.push_back(new Circle(Point(1, 2, 3), 7));

    v.push_back(new Car(Engine(200), Wheel(19), Wheel(19), Wheel(19), Wheel(19), 130));

    v.push_back(new WaterVehicle(5000));

    for (const auto& vehicle : v)
        cout << *vehicle << "\n";

    cout << "The highest power is " << getHighestPower(v) << '\n';


    for (const auto& vehicle : v)
        delete vehicle;

   
    v.clear();
    




}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
